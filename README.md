# Spring Test Architecture

This document is an overview of Spring / Java testing tools together with a suggested process for implementing BDD
using these tools.  

## Terminology

There are a number of collaborators in this development process: 

- **Product Owner:** Has the responsibility to define desired behaviour / functionality and sign off work produced
- **Tester:** Jointly responsible (with the developer) for testing the application and ensuring robustness 
Is a specialist in testing methodology and tooling.
- **Developer:** Responsible for developing functionality requested by the Product Owner
- **The Three Amigos:** All the above, in larger teams these tend to be designated from the pool of developers, testers and product owner. 

There are artifacts: 

- **Stories:** Used by product owners to encapsulate desired behaviour / functionality
- **Scenarios:** A story has one or more scenarios, written in Gherkin which formally define the behaviour of the story
      
## Principles

1. Stories and their scenarios created in conjunction with the customer should drive feature development 
1. Tests should drive the implementation of a scenario
1. All code should be covered with tests
1. It is not desirable to test all scenarios end to end but each scenario should be tested in the domain and it's integration
with spring and JPA should be covered  
1. Where possible we should lean on the power of Spring framework, however this should not lead us to create obtuse or 
hard to test code 

## Practises

### Overview

This flow diagram shows the process of letting stories, scenarios and tests drive the code produced. For more detail on how this 
workflow can be applied in Spring, see the sections below. 
[Click Here](https://www.evernote.com/l/Ai4QoW9RXlFIiolyN6W2cAH7wCV46022FcMB/image.png) to make it bigger.

![Testing Flow](https://www.evernote.com/l/Ai4QoW9RXlFIiolyN6W2cAH7wCV46022FcMB/image.png) 

### 1. Writing Scenarios

[This Article](https://cucumber.io/blog/2015/12/08/example-mapping-introduction) provides an in depth overview of 
"example mapping". Example mapping is a light weight technique that focuses on capturing a story, defining it's business
rules and examples. The technique is typically employed by 'the three amigos' (product owner, programmer and tester).  

The benefit of example mapping over sitting in a planning meeting and writing scenarios in Gherkin is that it focuses on 
a high quality discussion about requirements rather than perfecting gherkin syntax. Developers and Testers can then
work together to produce the gherkin after the mapping session, verifying the results with the PO. 

The scenarios in this project could have come from an example mapping like this: 

![ExampleMapping](https://www.evernote.com/l/Ai4b65sFgBJJ_a2kC7UrNp2Vm1YzbnMCJA4B/image.jpg)

### 2. Writing The First Test (end to end)

When using using the example mapping technique above it's likely to have produced a number of scenarios relating to the 
proposed feature. The first scenario is usually the simplest ("happy path") expression of the customers desired 
behaviour.

Create a feature file and a new test runner in a 'endtoend' test package. Add the story and the first scenario to the 
feature file and tag the scenario `@endtoend`. This test will be performed both end to end and as part of the unit 
level suite covering the domain. 

*Note: As a general rule only one scenario in a feature file should be @endtoend. This preserves the shape of the testing pyramid.*

The end to end test serves two purposes:

1. It's a focusing step, by concentrating on the feature from the outside in first, the test drives the feature development.  
1. When implementing subsequent scenarios in the group, it serves as a broad yard stick ensuring everything compiles and 
serves correctly.   

Features of an End to End test: 

1. Be bold - assert the request and response properties in full.
1. This test should be agnostic of the application so use JSONObject rather than RestTemplate<T>
1. Prefer stubbing over mocking  
 
*Feature File*

This is the first scenario in the feature file. Tagging end to end ensures it gets run as both as part of the end to end 
test run as well as in the domain suite (see bellow).

```gherkin
Feature: User Registration

  @endtoend
  Scenario: As a registering customer
    Given a customer exists with the name "Ben" and the email "ben@armakuni.com"
    When "Ben" registers
    Then a user account is created
```

*Test Configuration*

This configuration class uses the `@SpringBootTest` to point to our application context and to run the full stack,
binding the application to a random port. 

```java
package e2e;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import people.Application;

@ContextConfiguration
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class E2E {

}
```

*Test Runner*

The test runner serves as suite configuration - allowing us to specify which Gherkin to run and which tags to run. 
This file allows us to filter a small number of scenarios to be run 'end to end'. Finally, by default, only steps 
defined in this package are used to run the scenarios. This mechanism allows us to maintain multiple step definitions 
for end to end vs domain testing.    

```java
package e2e;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/cucumber", tags = "@endtoend")
public class Runner {
}
```

*Step Definitions*

In the end to end step definitions, notice that we examine the application entirely from the outside 
using a TestRestTemplate<String> to compare the JSON response with our expectations. 

```java 

package e2e;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;

import static org.assertj.core.api.Assertions.assertThat;

public class UserSteps extends E2E {

    @Autowired
    private TestRestTemplate restTemplate;

    private ResponseEntity<String> responseEntity;

    private String name;

    private String email;

    @When("^\"([^\"]*)\" registers with the email \"([^\"]*)\"$")
    public void registers_with_the_email(String arg1, String arg2) throws Throwable {

        this.name = name;
        this.email = email;

        JSONObject obj = new JSONObject();
        obj.put("name", name);
        obj.put("email", email);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        HttpEntity<String> entity = new HttpEntity<String>(obj.toString(), headers);

        responseEntity = restTemplate.postForEntity("/user", entity, String.class);

    }

    @Then("^a user account is created$")
    public void a_user_account_is_created() throws Throwable {

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);

        // Parse response for ID
        JSONObject json = new JSONObject(responseEntity.getBody());

        String debug = json.toString();

        assertThat(json.has("id"));

        Long id = json.getLong("id");
    }
}
```
**Now, run this test, it will fail, that's OK!** Code written in the following sections will make it pass.

When this test goes green, it demonstrates the domain and spring integration are successfully wired together into a 
functioning application.  
 
### 3. Write A Test In The Domain

Use the scenario added to the feature file in the last step.  

Create a new cucumber test runner in a `domain` test package. Implementing the step definitions for this test will guide 
you to create a service, repositories and domain objects.

1. This layer should be agnostic of the spring layers above, your test should mock and inject as appropriate
1. Use Interfaces to represent any repositories you create (the layer below)
1. When prompted by a step definition create a type and a method with the desired signature
1. Don't implement the method behaviour until you have a compiling, failing test

**Run this test, it will fail, thats OK!** 

Write just enough code to get the test to pass. This process of writting a test and making it fail is called the 
"Red, Green, Refactor Cycle".

This test should define the interface and behaviour of the domain layer as defined in the scenario. 
This includes method signatures and exceptions.

When integrating the domain with Spring we will be mocking Service, before moving on extract or update this service 
interface for use by the spring layer up.

**Run the end to end test now, it will still fail, thats OK!**. Although the domain has been defined it is 
not integrated in the application yet. Work done in later sections will ake this test pass. 

*Test Runner*

Note: in this runner we run all the features. Furthermore only the step definitions in this package will be used to run 
scenarios. In this package we can test the domain directly, in a framework agnostic way. 

```java
package people.domain;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources")
public class Runner {
}
```

*Step Definitions*

At this level we make use of the `@before` method to assemble the service and it's dependencies. Where possible we 
wire in real dependencies. EG - A real validator is used, however a mock repository is used to prevent costly DB access.  

```java
package people.domain;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class UserSteps {

    private UserService userService;

    private IUserRepository userRepository;

    private Validator validator;

    private Map<String, String> users;

    @Before
    public void setUp() {
        userRepository = mock(IUserRepository.class);
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
        userService = new UserService(userRepository, validator);
        users = new HashMap<String, String>();
    }

    @Given("^a customer has registered with the name \"([^\"]*)\" and the email \"([^\"]*)\"$")
    public void a_customer_has_registered_with_the_name_and_the_email(String name, String email) throws Throwable {

        a_customer_exists_with_the_name_and_the_email(name, email);
        User domainUser = new User(name, email);

        when(userRepository.findByEmail(email)).thenReturn(domainUser);
    }
    
    // More step definitions omitted for brevity
    
}
```

### 4. Defined a Repository In The Domain? Write A JPA Test.

Spring allows the developer to define an interface describing a CRUD repository and have it's implementation injected as a bean 
at run time. After the test above has driven out the signature of the repositories' interface it's worth covering this in a 
test. Spring provides `@DataJPATest` annotation for this purpose.  

This test ensures the target Entity is annotated correctly and that the method name complies with the DSL exposed by 
Spring Data.

### 5. Need Additional Granular Coverage?

When driving out the domain and service layers you may find it's useful to add additional tests which cover different
inputs that produce the same output. These can be a little verbose in gherkin.

Take the example of validating user input to the createUser method. There are many different ways an email address 
could be invalid, we can add further coverage by isolating the logic that validates email addresses and testing it 
directly with a parametrised JUnit test. In later versions of JUnit 'Dynamic Tests' could be used.

Rather than: 

```gherkin 
  Scenario: As a registering customer
    Given a customer exists with the name "Ben" and the email "beep.c"
    When "Ben" registers
    Then the "validation error" error should occur
    
  Scenario: As a registering customer
    Given a customer exists with the name "Ben" and the email "@hello.com"
    When "Ben" registers
    Then the "validation error" error should occur    
```    

Use A Parametrised Test: 

```java
package people.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

import static org.assertj.core.api.Java6Assertions.assertThat;

@RunWith(Parameterized.class)
public class UserDTOEmailTest {

    private Validator validator;

    @Before
    public void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Parameterized.Parameters
    public static Collection<Object[]> tests() {
        return Arrays.asList(new Object[][]{
                {"ben@armakuni.com", true},
                {"ben@sub.armakuni.com", true},
                {"b", false},
        });
    }

    private String email;

    private boolean shouldPass;

    public UserDTOEmailTest(String email, boolean shouldPass) {
        this.email = email;
        this.shouldPass = shouldPass;
    }

    @Test
    public void testEmailValidation() {

        UserDTO dto = new UserDTO();
        dto.email = email;

        Set<ConstraintViolation<UserDTO>> violations = validator.validate(dto);

        if (!shouldPass) {
            assertThat(violations).isNotEmpty();
            return;
        }
        
        assertThat(violations).isEmpty();
       
    }
}
```       

### 6. Write tests covering the integration with Spring

With a functioning domain, it can now be determined how it should integrate with spring. `@RestController`s are the main 
integration point and they can be driven out using Spring's `@WebMvc` annotation. Using this annotation will cause 
the controller to be loaded, together with the MVC but not any of the other `@components`. It also allows the developer 
to inject mocked dependencies into the IOC via the `@MockBean` annotation.  

This test should have a descriptive name and use technical language to describe the behaviour of the integration. 
For example, if building a REST webservice an example of a good test name could be: 
`createUser_ShouldReturnA201StatusCode_OnSuccess`. It clearly indicates what action is performed and the expected 
behaviour.

These tests: 

1. Have a technical audience (developers)
1. Should describe the implementation details of the integration (REST - status codes, headers, response bodies, authentication)
1. Should be granular, promoting excellent fault isolation
1. Should mock out the service layer using the interface created in the previous step

Ensure that for each technical requirement **a test is created, executed and fails**. The behaviour defined in the test is then 
added to the controller. Iterate until tests covering the various aspects of the integration are all green. 

*Controller Test*

This test uses `@WebMvcTest` to bootstrap the Application Context and to selectively inject only the WebMVC elements. 
This saves a costly start up and runs significantly quicker than an end to end test. Furthermore application contexts
are cashed and can be reused across controller tests. Using `@WebMvcTest` also allows the autowiring of a `MockMvc`, 
this is the Mock used to assert on the response generated by the controller.    

```java

package people.integration.web;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import people.domain.IUserService;
import people.domain.UserDTO;
import people.web.CreateUserController;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = CreateUserController.class)
public class CreateUserControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private IUserService userService;

    private MvcResult result;

    @Before
    public void setUp() throws Exception {
        UserDTO user = new UserDTO( new Long(1234),"Ben", "ben@email.com");
        given(userService.createUser(any(UserDTO.class))).willReturn(user);

        JSONObject json = new JSONObject();
        json.put("name", "Ben Andersen-Waine");
        json.put("email", "ben@armakuni.com");

        MockHttpServletRequestBuilder requestBuilder = post("/user");
        requestBuilder.content(json.toString());
        requestBuilder.contentType(MediaType.APPLICATION_JSON_UTF8);

        result = mvc.perform(requestBuilder).andReturn();
    }

    @Test
    public void createUser_ShouldReturnA201StatusCode_OnSuccess() throws Exception {
        assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.CREATED.value());
    }

    @Test
    public void createUser_ShouldReturnJSONMediaType() throws Exception {
        assertThat(result.getResponse().getContentType()).isEqualTo(MediaType.APPLICATION_JSON_UTF8.toString());
    }

    @Test
    public void createUser_ShouldReturnsAUserOnSuccess() throws Exception {

        JSONObject json = new JSONObject(result.getResponse().getContentAsString());

        assertThat(json.has("id"));
        assertThat(json.getLong("id")).isEqualTo(1234);

        assertThat(json.has("name"));
        assertThat(json.getString("name")).isEqualTo("Ben");

        assertThat(json.has("email"));
        assertThat(json.get("email")).isEqualTo("ben@email.com");
    }
}
```

### 7. Execute the End To End Test

Having written the domain and the spring integration the end to end test should now pass. If it doesn't the developer 
should isolate the issue, ensure it is covered by a failing test and implement a fix.      

### 8. Repeat

There is now a passing end to end test covering the most basic behaviour defined in the first scenario in a group of 
scenarios defined by the three amigos in the example mapping exercise above.

Resist the temptation to write another end to end test for each of the remaining scenarios, instead repeat the process 
from **step 3 - 7** until all the scenarios have been implemented in both the domain and the integration layers, the 
being behaviour driven out by tests. 

When starting a new feature add or update an end to end scenario and start again at **step 2**.

### To Do

1. Provide a good JPA example
1. Find a way to reliably test the API contract
 
## Resources

### Story Writing

- [Example Mapping](https://cucumber.io/blog/2015/12/08/example-mapping-introduction)
- [Dan North - What's In A Story](https://dannorth.net/whats-in-a-story/) 
- [Cucumber Anti Patterns 1](https://cucumber.io/blog/2016/07/01/cucumber-antipatterns-part-one)    
- [Cucumber Anti Patterns 2](https://cucumber.io/blog/2016/08/31/cucumber-anti-patterns-part-two)    

### Spring / Java Testing

- [Spring Boot Testing](https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-testing.html)
- [Test Section - Spring Manual](https://docs.spring.io/spring/docs/4.3.11.RELEASE/spring-framework-reference/htmlsingle/#testing)
- [Cucumber JVM](https://cucumber.io/docs/reference/jvm)
- [JUnit](http://junit.org/)
- [Mockito](http://site.mockito.org/)
