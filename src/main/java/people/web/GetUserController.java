package people.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import people.domain.IUserService;
import people.domain.UserDTO;

@RestController
public class GetUserController {

    @Autowired
    private IUserService userService;

    @RequestMapping(value = "/user/{userId}", method = RequestMethod.GET)
    public UserDTO getUser(@PathVariable Long userId) {
        return userService.getUser(userId);
    }

}
