package people.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import people.domain.IUserService;
import people.domain.UserDTO;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class CreateUserController {

    @Autowired
    private IUserService userService;

    @RequestMapping(path = "/user", method = POST)
    @ResponseStatus(HttpStatus.CREATED)
    public UserDTO postUser(@RequestBody UserDTO user) {
        return userService.createUser(user);
    }
}
