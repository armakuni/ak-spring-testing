package people.domain;

import org.springframework.data.repository.CrudRepository;

public interface IUserRepository extends CrudRepository<User, Long> {
    User findByEmail(String email);

    User findById(Long allocatedId);
}
