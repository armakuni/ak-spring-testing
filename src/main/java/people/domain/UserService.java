package people.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;

@Service
public class UserService implements IUserService {

    private IUserRepository repository;

    @Autowired
    private Validator validator;

    public UserService(IUserRepository repository, Validator validator) {
        this.repository = repository;
        this.validator = validator;
    }

    @Override
    public UserDTO createUser(UserDTO userData) {

        Set<ConstraintViolation<UserDTO>> violations = validator.validate(userData);

        if (!violations.isEmpty()) {
            throw new ValidationException();
        }

        User existingUser = repository.findByEmail(userData.email);

        if (existingUser != null) {
            throw new DuplicateUserException();
        }

        User user = new User(userData);

        repository.save(user);

        return new UserDTO(user);
    }

    @Override
    public UserDTO getUser(Long userId) {

        User user = repository.findById(userId);

        return new UserDTO(user);
    }
}
