package people.domain;

import org.hibernate.validator.constraints.Email;

public class UserDTO {

    public Long id;

    public String name;

    @Email
    public String email;

    protected UserDTO() {
    }

    public UserDTO(Long id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    public UserDTO(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public UserDTO(User user) {

        id = user.getId();
        name = user.getName();
        email = user.getEmail();
    }
}
