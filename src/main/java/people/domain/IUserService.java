package people.domain;

public interface IUserService {

    UserDTO createUser(UserDTO User);

    UserDTO getUser(Long id);

}
