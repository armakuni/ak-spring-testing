Feature: User Registration

  @endtoend
  Scenario: As a registering customer
    Given a customer exists with the name "Dave" and the email "dave@armakuni.com"
    When "Dave" registers
    Then a user account is created

  Scenario: As a previously registered customer
    Given a customer has registered with the name "Ben Dodd" and the email "ben@armakuni.com"
    And a customer exists with the name "Ben Andersen-Waine" and the email "ben@armakuni.com"
    When "Ben Andersen-Waine" registers
    Then the "duplicate user" error should occur

  Scenario: As a registering customer
    Given a customer exists with the name "Ben" and the email "beep"
    When "Ben" registers
    Then the "validation error" error should occur


