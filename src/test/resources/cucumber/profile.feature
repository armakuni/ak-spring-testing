Feature: User Profile
  As an authenticated user
  I can retrieve my profile
  So I can view my personal data

  @endtoend
  Scenario: As a logged in user
    Given a customer has registered with the name "Ben" and the email "ben@armakuni.com"
    When "Ben" retrieves his profile
    Then the name "Ben" is returned
    And the email "ben@armakuni.com" is returned