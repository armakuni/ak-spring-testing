package people.integration.web;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import people.domain.IUserService;
import people.domain.UserDTO;
import people.web.CreateUserController;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = CreateUserController.class)
public class CreateUserControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private IUserService userService;

    private MvcResult result;

    @Before
    public void setUp() throws Exception {
        UserDTO user = new UserDTO( new Long(1234),"Ben", "ben@email.com");
        given(userService.createUser(any(UserDTO.class))).willReturn(user);

        JSONObject json = new JSONObject();
        json.put("name", "Ben Andersen-Waine");
        json.put("email", "ben@armakuni.com");

        MockHttpServletRequestBuilder requestBuilder = post("/user");
        requestBuilder.content(json.toString());
        requestBuilder.contentType(MediaType.APPLICATION_JSON_UTF8);

        result = mvc.perform(requestBuilder).andReturn();
    }

    @Test
    public void createUser_ShouldReturnA201StatusCode_OnSuccess() throws Exception {
        assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.CREATED.value());
    }

    @Test
    public void createUser_ShouldReturnJSONMediaType() throws Exception {
        assertThat(result.getResponse().getContentType()).isEqualTo(MediaType.APPLICATION_JSON_UTF8.toString());
    }

    @Test
    public void createUser_ShouldReturnsAUserOnSuccess() throws Exception {

        JSONObject json = new JSONObject(result.getResponse().getContentAsString());

        assertThat(json.has("id"));
        assertThat(json.getLong("id")).isEqualTo(1234);

        assertThat(json.has("name"));
        assertThat(json.getString("name")).isEqualTo("Ben");

        assertThat(json.has("email"));
        assertThat(json.get("email")).isEqualTo("ben@email.com");
    }
}
