package people.integration.web;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import people.domain.IUserService;
import people.domain.UserDTO;
import people.web.GetUserController;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = GetUserController.class)
public class GetUserControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private IUserService userService;

    private MvcResult result;

    @Before
    public void setUp() throws Exception {

        UserDTO user = new UserDTO(1L, "Ben Andersen-Waine", "ben@armakuni.com");
        given(userService.getUser(eq(1L))).willReturn(user);

        MockHttpServletRequestBuilder requestBuilder = get("/user/1");

        result = mvc.perform(requestBuilder).andReturn();
    }

    @Test
    public void getUser_ShouldUseApplicationJSONContentType() {
        assertThat(result.getResponse().getHeader(HttpHeaders.CONTENT_TYPE)).isEqualTo(MediaType.APPLICATION_JSON_UTF8.toString());
    }

    @Test
    public void getUser_ShouldReturnA200StatusCode_OnSuccess() throws Exception {
        assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    public void getUser_ShouldReturnUser_OnSuccess() throws Exception {

        JSONObject json = new JSONObject(result.getResponse().getContentAsString());

        assertThat(json.has("id")).isTrue();
        assertThat(json.getLong("id")).isEqualTo(1);

        assertThat(json.has("name")).isTrue();
        assertThat(json.getString("name")).isEqualTo("Ben Andersen-Waine");

        assertThat(json.has("email")).isTrue();
        assertThat(json.getString("email")).isEqualTo("ben@armakuni.com");
    }

}
