package people.domain;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class UserSteps {

    private UserService userService;

    private IUserRepository userRepository;

    private Validator validator;

    private static String DUPLICATE_USER_ERROR = "duplicate user";

    private static String VALIDATION_ERROR = "validation error";

    private String error;

    private Map<String, User> scenarioUsers;

    private UserDTO currentUser;

    private Long idAllocation;

    @Before
    public void setUp() {
        userRepository = mock(IUserRepository.class);
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
        userService = new UserService(userRepository, validator);
        scenarioUsers = new HashMap<String, User>();
        idAllocation = 0L;
    }

    @Given("^a customer exists with the name \"([^\"]*)\" and the email \"([^\"]*)\"$")
    public void a_customer_exists_with_the_name_and_the_email(String name, String email) throws Throwable {
        User domainUser = new User(++idAllocation, name, email);
        scenarioUsers.put(name, domainUser);
    }

    @Given("^a customer has registered with the name \"([^\"]*)\" and the email \"([^\"]*)\"$")
    public void a_customer_has_registered_with_the_name_and_the_email(String name, String email) throws Throwable {

        a_customer_exists_with_the_name_and_the_email(name, email);
        Long allocatedId = idAllocation++;
        User domainUser = new User(allocatedId, name, email);

        when(userRepository.findByEmail(email)).thenReturn(domainUser);
        when(userRepository.findById(eq(allocatedId))).thenReturn(domainUser);
    }

    @When("^\"([^\"]*)\" registers$")
    public void registers(String name) throws Throwable {

        String email = scenarioUsers.get(name).getEmail();

        if (email == null) {
            throw new Exception("User not is users map, missing Given step?");
        }

        UserDTO user = new UserDTO(name, email);

        try {
            userService.createUser(user);
        } catch (DuplicateUserException e) {
            error = DUPLICATE_USER_ERROR;
            return;
        } catch (ValidationException e) {
            error = VALIDATION_ERROR;
            return;
        }

        User domainUser = new User(name, email);
        when(userRepository.findByEmail(email)).thenReturn(domainUser);
    }

    @When("^\"([^\"]*)\" retrieves his profile$")
    public void retrieves_his_profile(String name) throws Throwable {
        User scenarioUser = scenarioUsers.get(name);
        currentUser = userService.getUser(scenarioUser.getId());
    }

    @Then("^a user account is created$")
    public void a_user_account_is_created() throws Throwable {
        verify(userRepository).save(any(User.class));
    }

    @Then("^the \"([^\"]*)\" error should occur$")
    public void the_error_should_occur(String error) throws Throwable {
        assertThat(this.error).isEqualTo(error);
    }

    @Then("^the email \"([^\"]*)\" is returned$")
    public void the_email_is_returned(String email) throws Throwable {
        assertThat(currentUser.email).isEqualTo(email);
    }

    @Then("^the name \"([^\"]*)\" is returned$")
    public void the_name_is_returned(String name) throws Throwable {
        assertThat(currentUser.name).isEqualTo(name);
    }


}
