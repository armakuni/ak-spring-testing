package people.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

import static org.assertj.core.api.Java6Assertions.assertThat;

@RunWith(Parameterized.class)
public class UserDTOEmailTest {

    private Validator validator;

    @Before
    public void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Parameterized.Parameters
    public static Collection<Object[]> tests() {
        return Arrays.asList(new Object[][]{
                {"ben@armakuni.com", true},
                {"ben@sub.armakuni.com", true},
                {"b", false},
        });
    }

    private String email;

    private boolean shouldPass;

    public UserDTOEmailTest(String email, boolean shouldPass) {
        this.email = email;
        this.shouldPass = shouldPass;
    }

    @Test
    public void testEmailValidation() {

        UserDTO dto = new UserDTO();
        dto.email = email;

        Set<ConstraintViolation<UserDTO>> violations = validator.validate(dto);

        if (!shouldPass) {
            assertThat(violations).isNotEmpty();
            return;
        }

        assertThat(violations).isEmpty();

    }
}
