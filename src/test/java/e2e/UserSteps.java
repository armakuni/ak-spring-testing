package e2e;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;

import java.util.HashMap;

import static org.assertj.core.api.Assertions.assertThat;

public class UserSteps extends E2E {

    @Autowired
    private TestRestTemplate restTemplate;

    private ResponseEntity<String> response;

    private HashMap<String, String> scenarioUsers;

    private HashMap<String, User> apiUsers;

    private User currentUser;

    @Before
    public void setUp() {
        apiUsers = new HashMap<String, User>();
        scenarioUsers = new HashMap<String, String>();
    }

    @Given("^a customer exists with the name \"([^\"]*)\" and the email \"([^\"]*)\"$")
    public void a_customer_exists_with_the_name_and_the_email(String name, String email) throws Throwable {
        scenarioUsers.put(name, email);
    }

    @Given("^a customer has registered with the name \"([^\"]*)\" and the email \"([^\"]*)\"$")
    public void a_customer_has_registered_with_the_name_and_the_email(String name, String email) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        scenarioUsers.put(name, email);
        registers(name);
        a_user_account_is_created();
    }

    @When("^\"([^\"]*)\" registers$")
    public void registers(String name) throws Throwable {

        JSONObject obj = new JSONObject();
        obj.put("name", name);
        obj.put("email", getScenarioUserEmail(name));

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        HttpEntity<String> entity = new HttpEntity<String>(obj.toString(), headers);

        response = restTemplate.postForEntity("/user", entity, String.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    }

    @Then("^a user account is created$")
    public void a_user_account_is_created() throws Throwable {

        User user = handleUserResponse(response);

        apiUsers.put(user.name, user);
    }

    @When("^\"([^\"]*)\" retrieves his profile$")
    public void retrieves_his_profile(String name) throws Throwable {

        User user = getApiUser(name);

        response = restTemplate.getForEntity("/user/" + user.id.toString(), String.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        apiUsers.put(name, handleUserResponse(response));
    }

    @Then("^the name \"([^\"]*)\" is returned$")
    public void the_name_is_returned(String name) throws Throwable {
        assertThat(currentUser.name).isEqualTo(name);
    }

    @Then("^the email \"([^\"]*)\" is returned$")
    public void the_email_is_returned(String email) throws Throwable {
        assertThat(currentUser.email).isEqualTo(email);
    }

    private User handleUserResponse(ResponseEntity<String> jsonUser) throws Exception {

        JSONObject json = new JSONObject(jsonUser.getBody());

        Long id;
        String name;
        String email;

        try {
            assertThat(json.has("id"));
            id = json.getLong("id");

            assertThat(json.has("name"));
            name = json.getString("name");

            assertThat(json.has("email"));
            email = json.getString("email");
        } catch (JSONException e) {
            throw new Exception(e.getMessage() + " --  " + jsonUser.toString());
        }

        User user = new User(id, name, email);

        currentUser = user;

        return user;
    }

    private User getApiUser(String name) throws Exception {

        if (!apiUsers.containsKey(name)) {
            throw new Exception("Username not found");
        }

        return apiUsers.get(name);
    }

    private String getScenarioUserEmail(String name) throws Exception {

        if (!scenarioUsers.containsKey(name)) {
            throw new Exception("Username not found");
        }

        return scenarioUsers.get(name);
    }

    class User {

        Long id;

        String name;

        String email;

        User(Long id, String name, String email) {
            this.id = id;
            this.name = name;
            this.email = email;
        }
    }
}
